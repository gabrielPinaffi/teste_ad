using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Teste_AD_Web.Controllers;
using Teste_AD_Web.Models;

namespace Teste_AD_Web
{
    public class Startup
    {
        public static StreamWriter x;
        public static string CaminhoNome;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            CaminhoNome = "C:\\Users\\1690312\\source\\repos\\Teste_AD_Web\\Teste_AD_Web\\Arquivos Gerados\\Log\\ArquivoLog" + DateTime.Now.ToString("ddMMyyyyHHmmss")+".txt";
            x = File.CreateText(CaminhoNome);
            x.Close();
            EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Sistema Iniciado");
        }

        public static void EscreveNoLog(string texto)
        {
            x = File.AppendText(CaminhoNome);
            x.WriteLine(texto);
            x.Close();
        }

    public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApiContext>(opt => opt.UseInMemoryDatabase("InMemoryDB"));
            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            var context = serviceProvider.GetService<ApiContext>();
            AdicionarDadosTeste(context);
        }

        private static void AdicionarDadosTeste(ApiContext context)
        {
            var teste = new Models.Produto
            {
                Id = "1",
                descricao = "bola",
                categoria = "couro",
                vlUnitario = 10.00
            };
            context.Produtos.Add(teste);
            teste = new Models.Produto
            {
                Id = "2",
                descricao = "bola",
                categoria = "pl�stico",
                vlUnitario = 5.99
            };
            context.Produtos.Add(teste);
            teste = new Models.Produto
            {
                Id = "3",
                descricao = "Bal�o",
                categoria = "Festa",
                vlUnitario = 1.00
            };
            context.Produtos.Add(teste);

            //CLI 1
            var testecli = new Cliente
            {
                Id = "1",
                nomeCompleto = "Gabriel Pavani Pinaffi",
                cpf = "127.315.716-83",
                endereco = "R. Eduardo Benjamin Hosken, 173",
                cep = 86020440,
                bairro = "Bairro Concei��o",
                cidade = "Londrina",
                uf = "PR",
                telefone = "(43) 99118-6304"
            };
            context.Clientes.Add(testecli);

            var testeped = new Pedido
            {
                Id = "1",
                dtPedido = DateTime.Now,//Convert.ToDateTime("01/01/2020"),
                cliCodigo = "1",
                vlTotal = 80,
                vlFrete = 2
            };
            context.Pedidos.Add(testeped);

            var testeitensped = new Itens_do_Pedido
            {
                Id = "1",
                PedidoId = "1",
                codProduto = "1",
                vlTotal = 20,
                quantidade = 2
            };
            context.Itens_Pedidos.Add(testeitensped);

            testeitensped = new Itens_do_Pedido
            {
                Id = "2",
                PedidoId = "1",
                codProduto = "2",
                vlTotal = 5,
                quantidade = 1
            };
            context.Itens_Pedidos.Add(testeitensped);

            //CLI 2
            testecli = new Cliente
            {
                Id = "2",
                nomeCompleto = "Teste",
                cpf = "122.111.111-11",
                endereco = "R. Rua",
                cep = 99999999,
                bairro = "Bairro Bairro",
                cidade = "SP",
                uf = "SP",
                telefone = "(43) 99999-9999"
            };
            context.Clientes.Add(testecli);

            testeped = new Pedido
            {
                Id = "2",
                dtPedido = DateTime.Now,//Convert.ToDateTime("01/01/2020"),
                cliCodigo = "2",
                vlTotal = 1,
                vlFrete = 2
            };
            context.Pedidos.Add(testeped);

            testeitensped = new Itens_do_Pedido
            {
                Id = "3",
                PedidoId = "2",
                codProduto = "3",
                vlTotal = 1,
                quantidade = 1
            };
            context.Itens_Pedidos.Add(testeitensped);



            //INSERE FRETE na mem�ria
            var testefrete = new Frete
            {
                Id = "AC",
                nome = "Acre",
                inicioCep = 69900000,
                fimCep = 69999999,
                vlFrete = 62.75
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "AL",
                nome = "Alagoas",
                inicioCep = 57000000,
                fimCep = 57999999,
                vlFrete = 5.21
            };
            context.Fretes.Add(testefrete);
            
            testefrete = new Frete
            {
                Id = "AM",
                nome = "Amazonas",
                inicioCep = 69000000,
                fimCep = 69299999,
                vlFrete = 53.74
            };
            context.Fretes.Add(testefrete);
            testefrete = new Frete
            {
                Id = "AM2",
                nome = "Amazonas",
                inicioCep = 69400000,
                fimCep = 69899999,
                vlFrete = 53.74
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "AP",
                nome = "Amap�",
                inicioCep = 68900000,
                fimCep = 68999999,
                vlFrete = 44.12
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "BA",
                nome = "Bahia",
                inicioCep = 40000000,
                fimCep = 48999999,
                vlFrete = 24.36
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "CE",
                nome = "Cear�",
                inicioCep = 60000000,
                fimCep = 63999999,
                vlFrete = 33.52
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "DF",
                nome = "Bras�lia",
                inicioCep = 70000000,
                fimCep = 72799999,
                vlFrete = 19.54
            };
            context.Fretes.Add(testefrete);
            testefrete = new Frete
            {
                Id = "DF2",
                nome = "Bras�lia",
                inicioCep = 73000000,
                fimCep = 73699999,
                vlFrete = 19.54
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "ES",
                nome = "Esp�rito Santo",
                inicioCep = 29000000,
                fimCep = 29999999,
                vlFrete = 47.96
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "GO",
                nome = "Goi�s",
                inicioCep = 69300000,
                fimCep = 69399999,
                vlFrete = 19.89
            };
            context.Fretes.Add(testefrete);
            testefrete = new Frete
            {
                Id = "GO2",
                nome = "Goi�s",
                inicioCep = 73700000,
                fimCep = 76799999,
                vlFrete = 19.89
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "MA",
                nome = "Maranh�o",
                inicioCep = 65000000,
                fimCep = 65999999,
                vlFrete = 16.57
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "MG",
                nome = "Minas Gerais",
                inicioCep = 30000000,
                fimCep = 39999999,
                vlFrete = 0
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "MS",
                nome = "Mato Grosso do Sul",
                inicioCep = 79000000,
                fimCep = 79999999,
                vlFrete = 8.95
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "MT",
                nome = "Mato Grosso",
                inicioCep = 78000000,
                fimCep = 78899999,
                vlFrete = 14.59
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "PA",
                nome = "Par�",
                inicioCep = 66000000,
                fimCep = 68899999,
                vlFrete = 52.41
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "PB",
                nome = "Para�ba",
                inicioCep = 58000000,
                fimCep = 58999999,
                vlFrete = 19.84
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "PE",
                nome = "Pernambuco",
                inicioCep = 50000000,
                fimCep = 56999999,
                vlFrete = 42.76
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "PI",
                nome = "Piau�",
                inicioCep = 64000000,
                fimCep = 64999999,
                vlFrete = 29.65
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "PR",
                nome = "Paran�",
                inicioCep = 80000000,
                fimCep = 87999999,
                vlFrete = 24.24
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "RJ",
                nome = "Rio de Janeiro",
                inicioCep = 20000000,
                fimCep = 28999999,
                vlFrete = 24.65
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "RN",
                nome = "Rio grande do Norte",
                inicioCep = 59000000,
                fimCep = 59999999,
                vlFrete = 56.77
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "RO",
                nome = "Rond�nia",
                inicioCep = 76800000,
                fimCep = 76999999,
                vlFrete = 68.48
            };
            context.Fretes.Add(testefrete);
            testefrete = new Frete
            {
                Id = "RO2",
                nome = "Rond�nia",
                inicioCep = 78900000,
                fimCep = 78999999,
                vlFrete = 68.48
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "RR",
                nome = "Roraima",
                inicioCep = 69300000,
                fimCep = 69399999,
                vlFrete = 57.78
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "RS",
                nome = "Rio Grande do Sul",
                inicioCep = 90000000,
                fimCep = 99999999,
                vlFrete = 22.57
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "SC",
                nome = "Santa Catarina",
                inicioCep = 88000000,
                fimCep = 89999999,
                vlFrete = 16.45
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "SE",
                nome = "Sergipe",
                inicioCep = 49000000,
                fimCep = 49999999,
                vlFrete = 18.98
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "SP",
                nome = "S�o Paulo",
                inicioCep = 01000000,
                fimCep = 19999999,
                vlFrete = 2
            };
            context.Fretes.Add(testefrete);

            testefrete = new Frete
            {
                Id = "TO",
                nome = "Tocantins",
                inicioCep = 77000000,
                fimCep = 77999999,
                vlFrete = 8.59
            };
            context.Fretes.Add(testefrete);

            context.SaveChanges();
        }
    }
}
