﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Teste_AD_Web.Models
{
    public class Produto
    {
        public string Id { get; set; }
        public string descricao { get; set; }
        public string categoria { get; set; }
        public double vlUnitario { get; set; }
    }
}
