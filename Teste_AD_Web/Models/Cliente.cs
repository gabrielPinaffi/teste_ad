﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Teste_AD_Web.Models
{
    public class Cliente
    {
        public string Id { get; set; }
        public string nomeCompleto { get; set; }
        public string cpf { get; set; }
        public string endereco { get; set; }
        public int cep { get; set; }
        public string bairro { get; set; }
        public string cidade { get; set; }
        public string uf { get; set; }
        public string telefone { get; set; }
    }
}
