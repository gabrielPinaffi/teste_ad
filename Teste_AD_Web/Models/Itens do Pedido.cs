﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Teste_AD_Web.Models
{
    public class Itens_do_Pedido
    {
        public string Id { get; set; }
        public string PedidoId { get; set; }
        public string codProduto { get; set; }
        public double vlTotal { get; set; }
        public int quantidade { get; set; }
    }
}
