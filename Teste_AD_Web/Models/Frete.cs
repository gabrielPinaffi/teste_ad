﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Teste_AD_Web.Models
{
    public class Frete
    {
        public string Id { get; set; }
        public string nome { get; set; }
        public int inicioCep { get; set; }
        public int fimCep { get; set; }
        public double vlFrete { get; set; }
    }
}
