﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Teste_AD_Web.Models
{
    public class Pedido
    {
        public string Id { get; set; }
        public DateTime dtPedido { get; set; }
        //public int numero { get; set; }
        public string cliCodigo { get; set; }
        public double vlTotal { get; set; }
        public double vlFrete { get; set; }
        public List<Itens_do_Pedido> IPedido { get; set; }
    }
}
