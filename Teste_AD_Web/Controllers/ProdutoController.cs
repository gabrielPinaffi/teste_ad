﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Teste_AD_Web.Models;

namespace Teste_AD_Web.Controllers
{
    //[Produces("application/json")]
    //[Route("api/Produtos")]
    public class ProdutoController : Controller
    {
        private readonly ApiContext _context;
        public static Produto prodGlobal;

        public ProdutoController(ApiContext context)
        {
            _context = context;
        }
        
        [HttpGet]
        public IActionResult ListaProdutos()
        {
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Tela ListaProdutos chamada");
            return View(BuscaProdutos("-1"));
        }
        
        public List<Produto> BuscaProdutos(String id)
        {
            List<Produto> list = new List<Produto>();
            var resposta = _context.Produtos.Select(u => new
            {
                Id = u.Id,
                descricao = u.descricao,
                categoria = u.categoria,
                vlUnitario = u.vlUnitario
            });
            if (id=="-1") //Pega todos produtos
            {
                foreach (var p in resposta)
                {
                    Produto teste = new Produto();
                    teste.Id = p.Id;
                    teste.descricao = p.descricao;
                    teste.categoria = p.categoria;
                    teste.vlUnitario = p.vlUnitario;
                    list.Add(teste);
                }
            }
            else //Pega o em Questão
            {
                foreach (var p in resposta)
                {
                    if(p.Id.Equals(id))
                    {
                        Produto teste = new Produto();
                        teste.Id = p.Id;
                        teste.descricao = p.descricao;
                        teste.categoria = p.categoria;
                        teste.vlUnitario = p.vlUnitario;
                        list.Add(teste);
                        break;
                    }
                }
            }
            return list;
        }

        [HttpGet]
        public IActionResult CadastroProdutos()
        {
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Tela CadastroProdutos chamada");
            return View();
        }

        [HttpPost]
        public IActionResult CadastroProdutos(Produto prod)
        {
            if (BuscaProdutos(prod.Id).Count > 0)
                return View();
            var newProd = new Produto
            {
                Id = prod.Id,
                descricao = prod.descricao,
                categoria = prod.categoria,
                vlUnitario = prod.vlUnitario
            };
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Cadastro do Novo Produto(Id=" + newProd.Id + "descricao="+ newProd.descricao + "categoria=" + newProd.categoria + "vlUnitario=" + newProd.vlUnitario + ")");
            _context.Produtos.Add(newProd);

            _context.SaveChanges();

            return RedirectToAction("ListaProdutos");
        }

        [HttpGet]
        public IActionResult AlteraProduto(Produto prod)
        {
            return View(prod);
        }

        [HttpPost]
        public IActionResult AlterarProduto(Produto prod)
        {
            if (BuscaProdutos(prod.Id).Count == 0)
                return View();
            
            var newProd = new Produto
            {
                Id = prod.Id,
                descricao = prod.descricao,
                categoria = prod.categoria,
                vlUnitario = prod.vlUnitario
            };
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Alterar Produto(Id=" + prod.Id + "descricao=" + prod.descricao + "categoria=" + prod.categoria + "vlUnitario=" + prod.vlUnitario + ")");
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Alterado para Produto(Id=" + newProd.Id + "descricao=" + newProd.descricao + "categoria=" + newProd.categoria + "vlUnitario=" + newProd.vlUnitario + ")");
            _context.Produtos.Update(prod);

            _context.SaveChanges();

            return RedirectToAction("ListaProdutos");
        }

        [HttpGet]
        public IActionResult DeletaProduto(Produto c)
        {
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Tela DeletaCliente chamada");
            prodGlobal = c;
            return View(c);
        }

        [HttpPost]
        public IActionResult DeletaProduto()
        {
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Deletar Produto(Id=" + prodGlobal.Id + "descricao=" + prodGlobal.descricao + "categoria=" + prodGlobal.categoria + "vlUnitario=" + prodGlobal.vlUnitario + ")");
            _context.Produtos.Remove(prodGlobal);
            _context.SaveChanges();
            return RedirectToAction("ListaProdutos");
        }
        
    }
}
