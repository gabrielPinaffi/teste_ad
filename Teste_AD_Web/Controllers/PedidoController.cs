﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Teste_AD_Web.Models;

namespace Teste_AD_Web.Controllers
{
    public class PedidoController : Controller
    {
        private readonly ApiContext _context;
        public static List<Itens_do_Pedido> listaItensGlobal;
        public static Itens_do_Pedido ItemGlobal;
        public static Pedido pedGlobal;
        public static int tempID;

        public PedidoController(ApiContext context)
        {
            _context = context;
            if (pedGlobal == null)
                pedGlobal = new Pedido();
            if (listaItensGlobal == null)
                listaItensGlobal = new List<Itens_do_Pedido>();
            if (ItemGlobal == null)
                ItemGlobal = new Itens_do_Pedido();


        }
        
        [HttpGet]
        public IActionResult ListaPedidos()
        {
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Tela ListaPedidos chamada");
            pedGlobal = new Pedido();
            listaItensGlobal = new List<Itens_do_Pedido>();
            return View(BuscaPedidos("-1"));
        }

        [HttpGet]
        public IActionResult ListaMeusPedidos()
        {
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Tela ListaMeusPedidos chamada");
            tempID = 1;
            if (_context.Itens_Pedidos.Count() > 0)
                tempID = Convert.ToInt32(_context.Itens_Pedidos.Last().Id) +1;
            pedGlobal = new Pedido();
            listaItensGlobal = new List<Itens_do_Pedido>();

            if (HomeController.cliLogado.Id != null)
                return View(BuscaPedidos(HomeController.cliLogado.Id));
            return RedirectToAction("Login","Home");
        }

        [HttpGet]
        public IActionResult ListaItensPedido(Pedido p)
        {
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Tela ListaItensPedido chamada");
            var itens = BuscaItensPedido(p.Id);
            var prods = BuscaProdutos(itens);
            foreach(var prod in prods)
            {
                ViewData[prod.Id] = prod.categoria + " - " + prod.descricao;
            }
            return View(itens);
        }

        [HttpGet]
        public IActionResult ListaItensMeuPedido(Pedido p)
        {
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Tela ListaItensMeuPedido chamada");
            var itens = BuscaItensPedido(p.Id);
            var prods = BuscaProdutos(itens);
            foreach (var prod in prods)
            {
                ViewData[prod.Id] = prod.categoria + " - " + prod.descricao;
            }
            return View(itens);
        }

        [HttpGet]
        public IActionResult SelecionaItens()
        {
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Tela SelecionaItens chamada");
            ItemGlobal = new Itens_do_Pedido();
            return View(BuscaProdutos(null));
        }
        
        [HttpGet]
        public IActionResult AdicionarItens(Produto prod)//int l1, int l2, int l3)
        {
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Tela AdicionarItens chamada");
            ItemGlobal.Id = Convert.ToString(tempID++);
            ItemGlobal.codProduto = prod.Id;
            ItemGlobal.vlTotal = prod.vlUnitario;

            @ViewData["Produto"] = prod.categoria + " - " + prod.descricao;

            return View(ItemGlobal);
        }

        public IActionResult AdicioneItens(Itens_do_Pedido i)
        {
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Tela AdicioneItens chamada");
            int nID = 1;
            if (_context.Itens_Pedidos.Count() > 0)
                nID = Convert.ToInt32(_context.Itens_Pedidos.Last().Id) + 1;

            ItemGlobal.quantidade = i.quantidade;
            ItemGlobal.vlTotal = ItemGlobal.vlTotal * i.quantidade;
            listaItensGlobal.Add(ItemGlobal);

            return RedirectToAction("SelecionaItens");
        }

        [HttpGet]
        public IActionResult CadastroPedidos()
        {
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Tela CadastroPedidos chamada");
            int nID = 1;
            if (_context.Pedidos.Count() > 0)
                nID = Convert.ToInt32(_context.Pedidos.Last().Id) + 1;


            pedGlobal = new Pedido
            {
                Id = Convert.ToString(nID),
                dtPedido = DateTime.Now,
                cliCodigo = HomeController.cliLogado.Id,
                vlTotal = SomaValorTotalPedido(listaItensGlobal),//pedGlobal.vlTotal,
                vlFrete = CalculaFrete(HomeController.cliLogado.Id),
                IPedido = listaItensGlobal
            };
            
            for (int i=0; i<listaItensGlobal.Count();i++)
            {
                listaItensGlobal[i].PedidoId= pedGlobal.Id;
            }

            var prods = BuscaProdutos(pedGlobal.IPedido);
            foreach (var prod in prods)
            {
                ViewData[prod.Id] = prod.categoria + " - " + prod.descricao;
            }
            if (pedGlobal.IPedido.Count() == 0)
            {
                ViewData["erroItens"] = "Selecione pelo menos um Item para confirmar o Pedido!";
            }
            return View(pedGlobal);
        }

        public IActionResult CadastrarPedidos()
        {
            if(pedGlobal.IPedido.Count()>0)
            {
                pedGlobal.IPedido = null;
                Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Cadastro do Novo Pedido(Id=" + pedGlobal.Id + "dtPedido=" + pedGlobal.dtPedido + "cliCodigo=" + pedGlobal.cliCodigo + "vlTotal=" + pedGlobal.vlTotal + "vlFrete=" + pedGlobal.vlFrete + ")");
                _context.Pedidos.Add(pedGlobal);
                _context.SaveChanges();
                foreach (var i in listaItensGlobal)
                {
                    Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Cadastro do Novo Item de Pedido(Id=" + i.Id + "PedidoId=" + i.PedidoId + "codProduto=" + i.codProduto + "vlTotal=" + i.vlTotal + "quantidade=" + i.quantidade + ")");
                    _context.Itens_Pedidos.Add(i);
                    _context.SaveChanges();
                }
                return RedirectToAction("ListaMeusPedidos"); 
            }
            return RedirectToAction("CadastroPedidos");
        }

        [HttpGet]
        public IActionResult RemoveItem(Itens_do_Pedido i)
        {
            ItemGlobal = i;
            List<Itens_do_Pedido> li = new List<Itens_do_Pedido>();
            li.Add(i);
            var prod = BuscaProdutos(li);
            @ViewData["Produto"] = prod[0].categoria + " - " + prod[0].descricao;

            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Tela RemoveItem chamada");
            return View(ItemGlobal);
        }

        [HttpPost]
        public IActionResult RemoverItem(Itens_do_Pedido i)
        {
            Itens_do_Pedido aux = BuscaItem(i.Id);
            listaItensGlobal.Remove(aux);

            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Removido o Item(Id=" + aux.Id + "PedidoId=" + aux.PedidoId + "codProduto=" + aux.codProduto + "vlTotal=" + aux.vlTotal + "quantidade=" + aux.quantidade + ")");
            return RedirectToAction("CadastroPedidos");
        }


        [HttpGet]
        public IActionResult DeletaPedido(Pedido Ped)
        {
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Tela DeletaPedido chamada");
            pedGlobal = Ped;
            pedGlobal.IPedido = BuscaItensPedido(pedGlobal.Id);
            var prods = BuscaProdutos(pedGlobal.IPedido);
            foreach (var prod in prods)
            {
                ViewData[prod.Id] = prod.categoria + " - " + prod.descricao;
            }
            return View(pedGlobal);
        }
        [HttpPost]
        public IActionResult DeletarPedido(Pedido Ped)
        {
            _context.Pedidos.Remove(pedGlobal);
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Deletado o Pedido(Id=" + pedGlobal.Id + "dtPedido=" + pedGlobal.dtPedido + "cliCodigo=" + pedGlobal.cliCodigo + "vlTotal=" + pedGlobal.vlTotal + "vlFrete=" + pedGlobal.vlFrete + ")");
            foreach (var i in pedGlobal.IPedido)
            {
                Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Cadastro do Novo Item de Pedido(Id=" + i.Id + "PedidoId=" + i.PedidoId + "codProduto=" + i.codProduto + "vlTotal=" + i.vlTotal + "quantidade=" + i.quantidade + ")");
                _context.Itens_Pedidos.Remove(i);
            }
            _context.SaveChanges();
            return RedirectToAction("ListaMeusPedidos");
        }

        /*
        public void GeraNFE()
        {
            string nome = "";
            var lPedidosCli = BuscaPedidos(HomeController.cliLogado.Id);
            foreach (var p in lPedidosCli)
            {
                nome = "C:\\Users\\1690312\\source\\repos\\Teste_AD_Web\\Teste_AD_Web\\Arquivos Gerados\\Nota Fiscal\\NFe_IdPedido_" + p.Id + "_data_" + DateTime.Now.ToString("dd / MM / yyyy HH: mm: ss") + ".txt";
                StreamWriter sw = File.CreateText(nome);
                sw.Close();
                sw = File.AppendText("========================================");
                sw.WriteLine        ("===========Dados do cliente=============");
                sw.WriteLine("Nome="+HomeController.cliLogado.nomeCompleto + " | CPF=" + HomeController.cliLogado.cpf + " | CEP=" + HomeController.cliLogado.cep);
                sw.Close();
                
                 
                //NÃO DEU TEMPO PARA TERMINAR... COMENTAR TUDO :/
                 
                 
            }
        }*/

        /* 
         * 
         * 
         * MÉTODOS 
         *
         *
         */

        public List<Pedido> BuscaPedidos(String idCli)
        {
            List<Pedido> list = new List<Pedido>();
            var resposta = _context.Pedidos.Select(ped => new
            {
                Id = ped.Id,
                dtPedido = ped.dtPedido,
                cliCodigo = ped.cliCodigo,
                vlTotal = ped.vlTotal,
                vlFrete = ped.vlFrete,
                //IPedido = ped.IPedido
            });
            if (idCli == "-1") //Pega todos
            {
                foreach (var ped in resposta)
                {
                    Pedido temp = new Pedido();
                    temp.Id = ped.Id;
                    temp.dtPedido = ped.dtPedido;
                    //temp.numero = ped.numero;
                    temp.cliCodigo = ped.cliCodigo;
                    temp.vlTotal = ped.vlTotal;
                    temp.vlFrete = ped.vlFrete;
                    //lista_de_itens = ped.lista_de_itens.Select(p => new{ p.codProduto,p.vlTotal,p.quantidade})
                    list.Add(temp);
                }
            }
            else //Pega os Pedidos do cliente em Questão
            {
                foreach (var ped in resposta)
                {
                    if (ped.cliCodigo.Equals(idCli))
                    {
                        Pedido temp = new Pedido();
                        temp.Id = ped.Id;
                        temp.dtPedido = ped.dtPedido;
                        temp.cliCodigo = ped.cliCodigo;
                        temp.vlFrete = ped.vlFrete;
                        temp.vlTotal = ped.vlTotal;//SomaValorTotalPedido(ped.Id);
                        //temp.IPedido = BuscaItensPedido(ped.Id);
                        list.Add(temp);
                    }
                }
            }
            return list;
        }
        public List<Itens_do_Pedido> BuscaItensPedido(String idPed)
        {
            List<Itens_do_Pedido> list = new List<Itens_do_Pedido>();
            var resposta = _context.Itens_Pedidos.Select(ped => new
            {
                Id = ped.Id,
                PedidoId = ped.PedidoId,
                codProduto = ped.codProduto,
                quantidade = ped.quantidade,
                vlTotal = ped.vlTotal
            });
            foreach (var ped in resposta)
            {
                if (ped.PedidoId.Equals(idPed))
                {
                    Itens_do_Pedido temp = new Itens_do_Pedido();
                    temp.Id = ped.Id;
                    temp.PedidoId = ped.PedidoId;
                    temp.codProduto = ped.codProduto;
                    temp.quantidade = ped.quantidade;
                    temp.vlTotal = BuscaValorTotalItens(ped.codProduto, ped.quantidade);
                    list.Add(temp);
                }
            }

            return list;
        }
        public double BuscaValorTotalItens(String idProd, int quant)
        {
            var resposta = _context.Produtos.Select(ped => new
            {
                Id = ped.Id,
                vlUnitario = ped.vlUnitario
            });
            foreach (var p in resposta)
            {
                if (p.Id.Equals(idProd))
                {
                    return (p.vlUnitario * quant);
                }
            }

            return -1;
        }
        public double SomaValorTotalPedido(List<Itens_do_Pedido> iPed)
        {
            double Total = 0;
            foreach (var i in iPed)
            {
                Total += i.vlTotal;
            }

            return Total;
        }


        public double CalculaFrete(string cliCodigo)
        {
            Cliente cli = BuscaCliente(cliCodigo);
            if (cli == null)
                return -1;

            return BuscaFrete(cli);
        }
        public double BuscaFrete(Cliente c)
        {
            var resposta = _context.Fretes.Select(f => new
            {
                Id = f.Id,
                nome = f.nome,
                inicioCep = f.inicioCep,
                fimCep = f.fimCep,
                vlFrete = f.vlFrete
            });

            foreach (var f in resposta)
            {
                if (c.cep>=f.inicioCep && c.cep<=f.fimCep)//f.Id.Equals(id))
                {
                    return f.vlFrete;
                }
            }

            return -1;
        }
        public Cliente BuscaCliente(String id)
        {
            var resposta = _context.Clientes.Select(cli => new
            {
                Id = cli.Id,
                nomeCompleto = cli.nomeCompleto,
                cpf = cli.cpf,
                endereco = cli.endereco,
                cep = cli.cep,
                bairro = cli.bairro,
                cidade = cli.cidade,
                uf = cli.uf,
                telefone = cli.telefone
            });

            foreach (var p in resposta)
            {
                if (p.Id.Equals(id))
                {
                    Cliente temp = new Cliente();
                    temp.Id = p.Id;
                    temp.nomeCompleto = p.nomeCompleto;
                    temp.cpf = p.cpf;
                    temp.endereco = p.endereco;
                    temp.cep = p.cep;
                    temp.bairro = p.bairro;
                    temp.cidade = p.cidade;
                    temp.uf = p.uf;
                    temp.telefone = p.telefone;
                    return temp;
                }
            }

            return null;
        }


        public List<Produto> BuscaProdutos(List<Itens_do_Pedido> itens)
        {
            List<Produto> list = new List<Produto>();
            var resposta = _context.Produtos.Select(u => new
            {
                Id = u.Id,
                descricao = u.descricao,
                categoria = u.categoria,
                vlUnitario = u.vlUnitario
            });

            if(itens == null)
            {
                foreach (var p in resposta)
                {
                    Produto teste = new Produto();
                    teste.Id = p.Id;
                    teste.descricao = p.descricao;
                    teste.categoria = p.categoria;
                    teste.vlUnitario = p.vlUnitario;
                    list.Add(teste);
                }
            }
            else
            {
                foreach(var i in itens)
                {
                    foreach (var p in resposta)
                    {
                        if (i.codProduto.Equals(p.Id))
                        {
                            Produto teste = new Produto();
                            teste.Id = p.Id;
                            teste.descricao = p.descricao;
                            teste.categoria = p.categoria;
                            teste.vlUnitario = p.vlUnitario;
                            list.Add(teste);
                            break;
                        }
                    }
                }
            }

            return list;
        }

        public Itens_do_Pedido BuscaItem(String idItem)
        {
            foreach (var ped in listaItensGlobal)
            {
                if (ped.Id.Equals(idItem))
                {
                    return ped;
                }
            }

            return new Itens_do_Pedido();
        }
    }
}
