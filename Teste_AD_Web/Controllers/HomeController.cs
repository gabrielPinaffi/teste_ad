﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Teste_AD_Web.Models;

namespace Teste_AD_Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApiContext _context;
        private readonly ILogger<HomeController> _logger;
        public static Cliente cliLogado;

        public HomeController(ILogger<HomeController> logger, ApiContext context)
        {
            _context = context;
            _logger = logger;
            if(cliLogado == null)
                cliLogado = new Cliente();
        }
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(Cliente c)
        {
            if (c.cpf != null)
            {
                c.cpf = c.cpf.Insert(3, ".");
                c.cpf = c.cpf.Insert(7, ".");
                c.cpf = c.cpf.Insert(11, "-");

                Cliente temp = BuscaCliente(c.cpf);
                if (temp.Id != null)
                {
                    cliLogado = temp;//_context.Clientes.Where(x => x.cpf.Equals(c.cpf)).FirstOrDefault();
                    Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Login realizado Cliente(Id=" + cliLogado.Id +" CPF: "+ cliLogado.cpf + " Nome: " + cliLogado.nomeCompleto+")");
                    return RedirectToAction("Index");
                }
                else
                    ViewData["erro"] = "CPF inválido";
            }
            else
                ViewData["erro"] = "Por favor, Digite um CPF válido";
            return View();
        }

        public IActionResult Index()
        {
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Tela Index(HOME) chamada");
            return View(cliLogado);
        }
        /*
        [HttpPost]
        public IActionResult Index(Cliente c)
        {
            cliLogado = _context.Clientes.Where(x => x.Id == c.Id).FirstOrDefault();
            return View(cliLogado);
        }
        */

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public Cliente BuscaCliente(String cpf)
        {
            Cliente c = new Cliente();
            var resposta = _context.Clientes.Select(cli => new
            {
                Id = cli.Id,
                nomeCompleto = cli.nomeCompleto,
                cpf = cli.cpf,
                endereco = cli.endereco,
                cep = cli.cep,
                bairro = cli.bairro,
                cidade = cli.cidade,
                uf = cli.uf,
                telefone = cli.telefone
            });
            
            foreach (var p in resposta)
            {
                if (p.cpf.Equals(cpf))
                {
                    c.Id = p.Id;
                    c.nomeCompleto = p.nomeCompleto;
                    c.cpf = p.cpf;
                    c.endereco = p.endereco;
                    c.cep = p.cep;
                    c.bairro = p.bairro;
                    c.cidade = p.cidade;
                    c.uf = p.uf;
                    c.telefone = p.telefone;
                    break;
                }
            }
           
            return c;
        }

    }
}
