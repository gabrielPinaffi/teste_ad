﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Teste_AD_Web.Models;

namespace Teste_AD_Web.Controllers
{
    public class ClienteController : Controller
    {
        private readonly ApiContext _context;
        public static Cliente cliGlobal;

        public ClienteController(ApiContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult ListaClientes()
        {
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Tela ListaClientes chamada");
            return View(BuscaClientes("-1"));
        }

        public List<Cliente> BuscaClientes(String id)
        {
            List<Cliente> list = new List<Cliente>();
            var resposta = _context.Clientes.Select(cli => new
            {
                Id = cli.Id,
                nomeCompleto = cli.nomeCompleto,
                cpf = cli.cpf,
                endereco = cli.endereco,
                cep = cli.cep,
                bairro = cli.bairro,
                cidade = cli.cidade,
                uf = cli.uf,
                telefone = cli.telefone
            });
            if (id == "-1") //Pega todos
            {
                foreach (var cli in resposta)
                {
                    Cliente temp = new Cliente();
                    temp.Id = cli.Id;
                    temp.nomeCompleto = cli.nomeCompleto;
                    temp.cpf = cli.cpf;
                    temp.endereco = cli.endereco;
                    temp.cep = cli.cep;
                    temp.bairro = cli.bairro;
                    temp.cidade = cli.cidade;
                    temp.uf = cli.uf;
                    temp.telefone = cli.telefone;
                    list.Add(temp);
                }
            }
            else //Pega o em Questão
            {
                foreach (var p in resposta)
                {
                    if (p.Id.Equals(id))
                    {
                        Cliente temp = new Cliente();
                        temp.Id = p.Id;
                        temp.nomeCompleto = p.nomeCompleto;
                        temp.cpf = p.cpf;
                        temp.endereco = p.endereco;
                        temp.cep = p.cep;
                        temp.bairro = p.bairro;
                        temp.cidade = p.cidade;
                        temp.uf = p.uf;
                        temp.telefone = p.telefone;
                        list.Add(temp);
                        break;
                    }
                }
            }
            return list;
        }

        [HttpGet]
        public IActionResult CadastroClientes()
        {
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Tela CadastroClientes chamada");
            return View();
        }

        [HttpPost]
        public IActionResult CadastroClientes(Cliente cli)
        {
            if (BuscaClientes(cli.Id).Count > 0)
                return View();

            cli.cpf = cli.cpf.Insert(3, ".");
            cli.cpf = cli.cpf.Insert(7, ".");
            cli.cpf = cli.cpf.Insert(11, "-");

            cli.telefone = cli.telefone.Insert(0, "(");
            cli.telefone = cli.telefone.Insert(3, ")");
            cli.telefone = cli.telefone.Insert(4, " ");
            cli.telefone = cli.telefone.Insert(10, "-");

            int nID = 1;
            if (_context.Clientes.Count() > 0)
                nID = Convert.ToInt32(_context.Clientes.Last().Id) + 1;

            var newCli = new Cliente
            {
                Id = Convert.ToString(nID),
                nomeCompleto = cli.nomeCompleto,
                cpf = cli.cpf,
                endereco = cli.endereco,
                cep = cli.cep,
                bairro = cli.bairro,
                cidade = cli.cidade,
                uf = cli.uf.ToUpper(),
                telefone = cli.telefone
            };

            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Cadastro do Novo Cliente(Id=" + newCli.Id + " CPF: " + newCli.cpf + " Nome: " + newCli.nomeCompleto + " Endereco: " + newCli.endereco + " CEP: " + newCli.cep + " Bairro: " + newCli.bairro + " Cidade: " + newCli.cidade + " UF: " + newCli.uf+ " Telefone: " + newCli.telefone+")");
            _context.Clientes.Add(newCli);

            _context.SaveChanges();

            return RedirectToAction("ListaClientes");
        }

        [HttpGet]
        public IActionResult AlteraCliente(Cliente cli)
        {
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Tela AlteraCliente chamada");
            return View(cli);
        }

        [HttpPost]
        public IActionResult AlterarCliente(Cliente cli)
        {
            if (BuscaClientes(cli.Id).Count == 0)
                return View();

            cli.cpf = cli.cpf.Insert(3, ".");
            cli.cpf = cli.cpf.Insert(7, ".");
            cli.cpf = cli.cpf.Insert(11, "-");

            cli.telefone = cli.telefone.Insert(0, "(");
            cli.telefone = cli.telefone.Insert(3, ")");
            cli.telefone = cli.telefone.Insert(4, " ");
            cli.telefone = cli.telefone.Insert(10, "-");

            var newCli = new Cliente
            {
                Id = cli.Id,
                nomeCompleto = cli.nomeCompleto,
                cpf = cli.cpf,
                endereco = cli.endereco,
                cep = cli.cep,
                bairro = cli.bairro,
                cidade = cli.cidade,
                uf = cli.uf.ToUpper(),
                telefone = cli.telefone
            };
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Alterar Cliente(Id=" + cli.Id + " CPF: " + cli.cpf + " Nome: " + cli.nomeCompleto + " Endereco: " + cli.endereco + " CEP: " + cli.cep + " Bairro: " + cli.bairro + " Cidade: " + cli.cidade + " UF: " + cli.uf + " Telefone: " + cli.telefone + ")");
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Alterado para Cliente(Id=" + newCli.Id + " CPF: " + newCli.cpf + " Nome: " + newCli.nomeCompleto + " Endereco: " + newCli.endereco + " CEP: " + newCli.cep + " Bairro: " + newCli.bairro + " Cidade: " + newCli.cidade + " UF: " + newCli.uf + " Telefone: " + newCli.telefone + ")");
            _context.Clientes.Update(newCli);

            _context.SaveChanges();

            return RedirectToAction("ListaClientes");
        }

        [HttpGet]
        public IActionResult DeletaCliente(Cliente c)
        {
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Tela DeletaCliente chamada");
            cliGlobal = c;
            return View(c);
        }
        [HttpPost]
        public IActionResult DeletaCliente()
        {
            Startup.EscreveNoLog(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Deletar Cliente(Id = " + cliGlobal.Id + " CPF: " + cliGlobal.cpf + " Nome: " + cliGlobal.nomeCompleto + " Endereco: " + cliGlobal.endereco + " CEP: " + cliGlobal.cep + " Bairro: " + cliGlobal.bairro + " Cidade: " + cliGlobal.cidade + " UF: " + cliGlobal.uf + " Telefone: " + cliGlobal.telefone + ")");
            _context.Clientes.Remove(cliGlobal);
            _context.SaveChanges();
            return RedirectToAction("ListaClientes");

        }

    }
}
