1.	Abrir e executar a aplicação no Visual Studio 2019;
2.	Foram criadas as telas MVC na aplicação o que possibilita a realização dos testes diretamente nas telas.
3.	
-> Para o funcionamento correto do log em sua máquina alterar o caminho de gravação do arquivo de log. Esta variável pode ser encontrada no arquivo "Startup.cs" linha 27, conforme exemplo abaixo:
Ex:. CaminhoNome = "C:\\Users\\1690312\\source\\repos\\Teste_AD_Web\\Teste_AD_Web\\Arquivos Gerados\\Log\\ArquivoLog" + DateTime.Now.ToString("ddMMyyyyHHmmss")+".txt";
	
OBS.: Caso seja feita configuração conforme exemplo acima o log será gerado e atualizado enquanto o sistema estiver rodando na pasta "Log" localizada dentro da pasta "Arquivos Gerados" que por sua vez esta localizada na pasta Raiz do sistema.
	
-> Ao iniciar o sistema são cadastradas algumas informações para facilitar os testes iniciais. Cadastradas informações tais como:
	- Produto(Id,descricao,categoria,vlUnitario) valores (1,bola,couro,10)
	- Produto(Id,descricao,categoria,vlUnitario) valores (2,bola,plastico,5.99)
	- Produto(Id,descricao,categoria,vlUnitario) valores (3,Balão,Festa,1)
	- Cliente(Id,nomeCompleto,cpf,endereco,cep,bairro,cidade,uf,telefone) valores (1,Gabriel Pavani Pinaffi,127.315.716-83,"R. Eduardo Benjamin Hosken, 173", 86020440, Bairro Conceição, Londrina, PR, (43) 99118-6304)
	- Pedido(Id,dtPedido,cliCodigo,vlTotal,vlFrete) values (1,DateTime.Now,1,80,2)
	- Itens_do_Pedido(Id,PedidoId,codProduto,vlTotal,quantidade) values (1,1,1,20,2)
	- Itens_do_Pedido(Id,PedidoId,codProduto,vlTotal,quantidade) values (2,1,2,5,1)
	- Cliente(Id,nomeCompleto,cpf,endereco,cep,bairro,cidade,uf,telefone) values (2,Teste,122.111.111-11,R. Rua,99999999,Bairro Bairro,SP,SP,(43) 99999-9999)
	- Pedido(Id,dtPedido,cliCodigo,vlTotal,vlFrete) values (2,DateTime.Now,2,1,2)
	- Itens_do_Pedido(Id,PedidoId,codProduto,vlTotal,quantidade) values (3,2,3,1,1)
		
OBS.: A inserção esta sendo realizada no arquivo "Startup.cs" no método "private static void AdicionarDadosTeste(ApiContext context)" 
	
1. Para testes de Criação, Alteração e Exclusão de Pedidos é necessário realizar o LOGIN com o cpf de um cliente cadastrado. 
1.2 Para realizar o Login basta clicar no botão "Logar" localizado na barra de tarefas, do lado direito (canto direito superior da tela) e preencher com um CPF válido.
1.3 Após login, Clicar no campo Pedidos > Meus pedidos.
1.4 Nesta tela ficarão listados seus pedidos realizados e o botão "Novo Pedido".
1.4.1 Na tela "Selecione o(s) Item(s) do seu Pedido"
1.4.2 Clicar no icone de carrinho
1.4.2.1 Na tela "Pedir Item" 
1.4.2.2 Digitar a quantidade deste produto que deseja pedir
1.4.2.3 Clicar em "Save"
1.4.3 Realizar este passo (1.4.2) para todos os produtos que deseja pedir 
1.4.4 Após cadastrar todos os itens desejados, clicar no botão "Fazer Pedido".
1.4.4.1 Na tela "Novo Pedido"
1.4.4.2 Validar os itens e, se desejar, remover algum item clicando na lixeira (lado direito da Tabela)
1.4.4.2.1 Na tela "Remover Item"
1.4.4.2.1.1 Clicar em "Save" para aceitar a exclusão.
1.4.4.2.1.2 Clicar em Voltar para cancelar exclusão.
1.4.4.3 Caso necessite ou deseje adicionar outros produtos, clicar em voltar e realizar do passo 1.4.1 em diante.
1.4.4.4 Clicar em "Gerar Pedido" para cadastrar o pedido e seus Itens, Sendo direcionado para a tela "Meus Pedidos".